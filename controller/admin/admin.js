// import config from 'config-lite'
import crypto from 'crypto'
import AdminModel from '../../models/admin/admin'
import AddressComponent from '../../prototype/addressComponent'
import dtime from 'time-formater'
import chalk from 'chalk';

class Admin extends AddressComponent {
    constructor() {
        super()
        this.login = this.login.bind(this)
        this.encryption = this.encryption.bind(this)
    }

    // 登录
    async login(req, res, next) {
        const newpassword = this.encryption(req.body.pwd); // 密码加密
        const user_name = req.body.user; //用户名
        try {
            // 查询是否有这个用户存在
            const admin = await AdminModel.findOne({
                user_name
            })
            const admin_id = await this.getId('admin_id');
            // 没有就注册
            if (!admin) {
                const status = 1;
                const adminTip = status == 1 ? '管理员' : '超级管理员';
                const cityInfo = "广东";
                const newAdmin = {
                    user_name,
                    password: newpassword,
                    id: admin_id,
                    create_time: dtime().format('YYYY-MM-DD HH:mm'),
                    admin: adminTip,
                    status,
                    city: cityInfo
                }
                // 添加用户到数据库
                await AdminModel.create(newAdmin)
                req.session.admin_id = admin_id;
                console.log("注册管理员成功");
                res.json({
                    status: 1,
                    message: '注册管理员成功',
                })
            } else if (newpassword.toString() !== admin.password.toString()) {
                console.log("管理员登录密码错误");
                res.json({
                    status: 0,
                    message: '该用户已存在，密码输入错误'
                });
            } else {
                console.log(admin)
                req.session.admin_id = admin.id;
                res.json({
                    status: 1,
                    message: '登录成功'
                });
            }
        } catch (err) {
            console.log("登录管理员失败", err);
            res.send({
                status: 0,
                message: '登录管理员失败'
            })
        }
    }
    // 密码加密
    encryption(password) {
        const newpassword = this.Md5(this.Md5(password).substr(2, 7) + this.Md5(password));
        return newpassword
    }
    // md5密码处理
    Md5(password) {
        const md5 = crypto.createHash('md5');
        return md5.update(password).digest('base64');
    }

    // 获取用户信息
    async getAdminInfo(req, res, next) {
        console.log(req.session.admin_id)
        const admin_id = req.session.admin_id;
        if (!admin_id || !Number(admin_id)) {
            // console.log('获取管理员信息的session失效');
            res.send({
                status: 0,
                type: 'ERROR_SESSION',
                message: '获取管理员信息失败'
            })
            return
        }
        try {
            const info = await AdminModel.findOne({
                id: admin_id
            }, '-_id -__v -password');
            if (!info) {
                throw new Error('未找到当前管理员')
            } else {
                res.send({
                    status: 1,
                    data: info
                })
            }
        } catch (err) {
            console.log('获取管理员信息失败');
            res.send({
                status: 0,
                type: 'GET_ADMIN_INFO_FAILED',
                message: '获取管理员信息失败'
            })
        }
    }
}

export default new Admin();