# node-elm

#### 项目介绍

仿饿了么后端服务器

#### 软件架构

- config 公共元素
- controller 接口实例
- mongodb 数据库
- routes 路由
- app.js 入口文件
- index.js 默认启动文件

#### 安装教程

1. sudo git clone https://gitee.com/yhf7/myelm.git
2. cd myelm
3. sudo cnpm i (安装插件，npm 等都可以)

#### 使用说明

1. 开启本地的 mongodb 数据库
2. 进入项目
3. sudo npm run start 启动项目

#### 有问题可添加微信或 qq 留言

1. 微信：13713313482
2. qq： 905477376
3. [博客：](https://yhf7.github.io/)https://yhf7.github.io/

#### 技术支撑

- es6+mongoose+node.js+express

#### 后记
- 如有侵权请及时联系小编删除，只是个人的学习模仿
