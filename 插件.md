# node 后端服务器开发插件

## 插件目录 [插件官网](https://www.npmjs.com/)
- ES2015（es6）
- express node的前端模板
- nodemon node改变监听
- config-lite 1.5.0 文件的遍历
- chalk 粉笔插件变色
- mongoose mongodb使用插件
- body-parser 配置解析表单 POST 请求体插件
- time-formater 时间插件


## ES2015
- 都是用来处理es6的兼容的直接放json文件里面安装就好想装新版本也可以一个一个装
``` javascript
"babel": "^6.23.0",
"babel-cli": "^6.26.0",
"babel-core": "^6.24.0",
"babel-preset-es2015": "^6.24.1",
"babel-preset-stage-3": "^6.22.1",
"babel-register": "^6.24.0",
"babel-plugin-transform-async-to-generator": "^6.24.1",
"babel-plugin-transform-es2015-classes": "^6.24.1",
"babel-plugin-transform-es2015-modules-commonjs": "^6.24.1",
"babel-plugin-transform-export-extensions": "^6.22.0"
```
- .babelrc 配置es6
``` javascript
{
    "presets": ["stage-3"],
    "plugins": [
        "transform-async-to-generator",
        "transform-es2015-modules-commonjs",
        "transform-export-extensions"
    ]
}
```

- index.js （默认启动文件）
```javascript
require('babel-core/register');
require('./app.js'); // 被调用的入口文件
```

## express 
- node的前端开发模板

1. 安装
``` bash
$ sudo cnpm i express -S
```
2. 使用
``` javascript
import Express from 'express'; //引入
const app = Express(); //定义

app.all('/*', (req, res, next) => { //设置跨域请求报文头
    //设置允许跨域响应报文头
    //设置跨域
    // 启用 Node 服务器端的 cors 跨域
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods", "*");

    res.setHeader('Content-Type', 'application/json;charset=utf-8');
    next();
});

app.get('/', (req, res) => { // 路由判断
    res.send(`hello world!`);
});

app.listen(5000, () => { //开启服务
    console.log('server running http://localhost:' + 5000);
});
```

## nodemon 
- nodemon 是node的改变监听 ，只要改变就会重新编译，就不需要每次改变都手动改变 （也可以作为全局安装）
1. 安装
``` bash
$ sudo cnpm i nodemon -S 本地安装
$ sudo cnpm i nodemon -g 全局安装
```
2. 使用
- 本地安装使用 在 package.json 定义 
    ``` javascript
    "scripts": {
        "start": "nodemon index.js"
    }
    ```
    - 终端 sudo npm run start调用
- 全局方法 直接在终端 sudo nodemon index.js


## config-lite 1.5.0
- 这是一个用了遍历查找文件的插件，其实简单的来说就是用来定义一些公共的参数，调用的时候方便一点
1. 安装
``` bash
$ sudo cnpm i config-lite@1.5 -S
```
2. 配置
    - 默认是查找config下的default.js文件（为了用起来方便我们就不改变直接定义）
    - 在项目目录新建 config文件夹 在里面建立 default.js文件 然后定义一些公共的元素或者是可变的元素
    ``` javascript
    'use strict';

    module.exports = {
        port: 5000,
        url: 'mongodb://localhost/myelm',
        session: {
            name: 'SID',
            secret: 'SID',
            cookie: {
                httpOnly: true,
                secure: false,
                maxAge: 365 * 24 * 60 * 60 * 1000,
            }
        }
    }
    ```
3. 调用
- 调用就比较简单
``` javascript
import config from 'config-lite'; //引入

config.url  //好像用对象那样使用，不过要你返回的是个对象才可以
```

## chalk
- 这个插件主要是用了改变输出的颜色，在不同的情况定义输出的字体颜色
1. 安装
``` bash
$ sudo cnpm i chalk -S
```
2. 使用
``` javascript
import chalk from 'chalk'; //引入
chalk.yellow('连接数据库成功') // 直接 chalk.颜色(变色内容)
```


## mongoose [官方](https://www.npmjs.com/package/mongoose)
- 数据库就自己装这里不说，直接安装全局的
- 这是一个mongodb数据库的开发插件
1. 安装
``` bash
$ sudo cnpm i mongoose -S
```
2. 使用
``` javascript
import mongoose from 'mongoose'; //引入

mongoose.connect('mongodb://localhost/myelm', {  //连接数据库
    useNewUrlParser: true
});

const db = mongoose.connection;

db.once('open', () => {
    console.log(
        chalk.yellow('连接数据库成功')
    );
});

db.on('error', function (error) {
    console.error(
        chalk.red('Error in MongoDb connection: ' + error + ' 数据库有问题')
    );
    mongoose.disconnect();
});

db.on('close', function () {
    console.log(
        chalk.red('数据库断开，重新连接数据库')
    );
    mongoose.connect(config.url, {
        server: {
            auto_reconnect: true
        }
    });
});

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
 
const BlogPost = new Schema({  //定义数据库
  author: ObjectId,
  title: String,
  body: String,
  date: Date
});
```

## body-parser
- 这个是解析表单post请求体的插件 
1. 安装
``` bash
$ sudo cnpm i body-parser -S
```
2. 配置
``` javascript
// 在主页定义 一定要在路由判断之前才能生效
// 配置解析表单 POST 请求体插件 （注意：一定要在 app.use(router) 之前）
// parse application/x-www-form-urlencoded 解析application
app.use(bodyParser.urlencoded({
    extended: false
}));
// parse application/json 解析
app.use(bodyParser.json());
```
3. 调用
- 调用就很简单不用引入，传过来的数据解析在req.body里面
``` javascript
app.post('/api/ts',(req,res) => {
    console.log(req.body);
})
```

## time-formater
- 这个一个js的解析时间的插件来的
1. 安装
``` bash
$ sudo cnpm i time-formater -S
```
2. 使用
``` javascript
import dtime from "time-formater";

let rawDate = dtime(new Date).format('YYYY-MM-DD HH:mm:ss') // 当前时间
console.log(rawDate)
```