'use strict';

module.exports = {
	port: 1001,
	url: 'mongodb://localhost/myelm',
	session: {
		name: 'SID',
		secret: 'SID',
		cookie: {
			httpOnly: true,
			secure: false,
			maxAge: 365 * 24 * 60 * 60 * 1000,
		}
	}
}
