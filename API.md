# node-elm 接口文档
```

baseUrl: http://localhost:5000

```

## 目录：

### 后台部分
[1、管理员登录](#1管理员登录)<br/>



## 接口列表：
### 后台部分

### 1、管理员登录

#### 请求URL：
```
https://elm.cangdu.org/admin/login
```

#### 示例：


#### 请求方式：
```
POST
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_name      |Y       |string   | 用户名 |
|password      |Y       |string  | 密码 |



#### 返回示例：

```javascript

{
  status: 1,
  success: '登录成功'
}
```